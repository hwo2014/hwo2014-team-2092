var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    send({
      msgType: "throttle",
      data: 0.5
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'yourCar') {
      console.log('yourCar');
    } else if (data.msgType === 'gameInit') {
      console.log('gameInit');
    } else if (data.msgType === 'tournamentEnd') {
      console.log('tournamentEnd');
    } else if (data.msgType === 'crash') {
      console.log('crash');
    } else if (data.msgType === 'spawn') {
      console.log('spawn');
    } else if (data.msgType === 'lapFinished') {
      console.log('lapFinished');
    } else if (data.msgType === 'dnf') {
      console.log('dnf');
    } else if (data.msgType === 'finish') {
      console.log('finish');
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
